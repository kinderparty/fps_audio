﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MixEvents : MonoBehaviour
{
    public AudioMixer masterMixer;


    void Start()
    {
        MixManager.current.onBlasterMixEvent += OnBlasterMixEventStart;
    }

    private void OnBlasterMixEventStart()
    {
        //mix groups to duck
        StopAllCoroutines();
        StartCoroutine(DuckGroup.StartDuck(masterMixer, "ambientVolume", 0, 0.05f, 0.5f, 1f));
        //StartCoroutine(FadeMixerGroup.StartFade(masterMixer, "ambientVolume", 0.3f, 0));
        
    }

    //mix events listed here
}
