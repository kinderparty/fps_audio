﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public static class DuckGroup
{
    static float currentVol = -1;
    //mixer, group param, duck amount, attack dur, hold dur, release dur
    public static IEnumerator StartDuck(AudioMixer audioMixer, string exposedParam, float targetVolume, float attackDuration, float holdDuration, float releaseDuration)
    {
        float currentTime = 0;

        if (currentVol < -0.1)
        {
            audioMixer.GetFloat(exposedParam, out currentVol);
            currentVol = Mathf.Pow(10, currentVol / 20);
        }
        float targetValue = Mathf.Clamp(targetVolume, 0.0001f, 1);

        while (currentTime < attackDuration)
        {
            currentTime += Time.deltaTime;
            float newVol = Mathf.Lerp(currentVol, targetValue, currentTime / attackDuration);
            audioMixer.SetFloat(exposedParam, Mathf.Log10(newVol) * 20);
            yield return null;
        }
        while (currentTime >= attackDuration && currentTime <(attackDuration+holdDuration))
        {
            currentTime += Time.deltaTime;
            audioMixer.SetFloat(exposedParam, Mathf.Log10(targetValue) * 20);
            yield return null;
        }
        while (currentTime >= (attackDuration+holdDuration) && currentTime <= (attackDuration + holdDuration + releaseDuration))
        {
            currentTime += Time.deltaTime;
            float newVol = Mathf.Lerp(targetValue, currentVol, currentTime / releaseDuration);
            audioMixer.SetFloat(exposedParam, Mathf.Log10(newVol) * 20);
            yield return null;
        }
        currentVol = -1;
        yield break;
    }
}