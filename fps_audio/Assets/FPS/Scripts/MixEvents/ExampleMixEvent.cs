﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class ExampleMixEvent : MonoBehaviour
{
    public AudioMixer masterMixer;

    public void MuteAmbient()
    {
        masterMixer.SetFloat("ambientVolume", Mathf.Log(0.001f) * 20);
    }

    private void Start()
    {
        masterMixer.SetFloat("ambientVolume", Mathf.Log(0.001f) * 20);
    }
}

