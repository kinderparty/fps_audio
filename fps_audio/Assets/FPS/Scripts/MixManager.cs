﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixManager : MonoBehaviour
{
    public static MixManager current;

    private void Awake()
    {
        current = this;
    }

    public event Action onBlasterMixEvent;
    public void BlasterMixEvent()
    {
        if (onBlasterMixEvent != null)
        {
            onBlasterMixEvent();
        }
    }


}
